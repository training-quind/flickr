package models;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class Credentials {

    //private ReadCSVFile readCSVFile = new ReadCSVFile();

    //@CsvBindByPosition(position = 0)
    @CsvBindByName(column = "email")
    public String email;
    //@CsvBindByPosition(position = 1)
    @CsvBindByName(column = "password")
    public String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Credential{" + "email= " + email + '\'' + ", password='" + password + '\'' + '}';
    }


}



