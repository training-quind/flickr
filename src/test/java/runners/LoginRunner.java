package runners;


//import io.cucumber.junit.CucumberOptions;
//import models.Credentials;
//import models.ReadCSVFile;
import io.restassured.http.ContentType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;
//import ui.HomePage;

import java.util.HashMap;
import java.util.Map;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;


@RunWith(SerenityRunner.class)
/*@CucumberOptions(
        features = "src/test/resources/features/",
        glue = "/stepdefinitions",
        tags = "",
        snippets = CucumberOptions.SnippetType.CAMELCASE
)*/
public class LoginRunner {


    @Test
    public void userAlreadyHasAFlickrAccount() {

        Map<String, String> params = new HashMap<String, String>();
        //params.put("method", "flickr.galleries.getList");
        //params.put("api_key", "5f0d2221a085fa8b661905738c5b0526");
        params.put("format", "json");
        params.put("nojsoncallback", "1");
        //params.put("oauth_token","72157720828805265-b061913ffb29d500" );
        //params.put("oauth_consumer_key","831cc477ed707aeec312e77126b09ea2");
        params.put("oauth_verifier","669c9b88dc06c3a2");
        params.put("oauth_signature_method","HMAC-SHA1");
        params.put("oauth_signature","p5araudg2jknjknkbIogdCPHY%3D");
        params.put("oauth_nonce","Slu7D3uQAow");
        params.put("oauth_timestamp","1642295415");
        params.put("oauth_version","1.0");

        Actor john = Actor.named("John")
                .whoCan(CallAnApi.at("https://www.flickr.com/services"));
        try {
            john.attemptsTo(
                Get.resource("/?method=flickr.galleries.getList&api_key=5f0d2221a085fa8b661905738c5b0526&format=json&nojsoncallback=1&api_sig")
                        .with(
                                request -> request
                                        .auth().oauth("831cc477ed707aeec312e77126b09ea2","908f30d27b427ad6","72157720828790789-946065b0c37cced0", "0e31a0310b7239aa")
                                        .accept(ContentType.JSON)
                                        .queryParams(params)
                                        .header("Connection", "keep-alive")
                        )



                );




        } catch (ExceptionInInitializerError e) {
            throw new ExceptionInInitializerError(e);
        }

        //assertThat(SerenityRest.lastResponse().statusCode()).isEqualTo(200);

            john.should(
                seeThatResponse("the correct id must be returned",
                        response -> response.statusCode(200)
                                .body("galleries.user_id", equalTo("194539726@N04"))
                )
        );
    }


}
