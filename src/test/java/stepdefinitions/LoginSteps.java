/*package stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.Credentials;
import models.ReadCSVFile;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import tasks.Login;
import ui.HomePage;

import static org.assertj.core.api.Assertions.assertThat;


public class LoginSteps {
    @Managed(driver = "chrome")
    private WebDriver navigator;
    private Actor john = Actor.named("John");
    private HomePage homepage = new HomePage();
    private Credentials credential = new Credentials();
    private ReadCSVFile readCSVFile = new ReadCSVFile();
    private String theRestApiBaseUrl = "https://www.flickr.com/services/rest";



    @Given("user already has a Flickr account.")
    public void userAlreadyHasAFlickrAccount() {
        john.can(BrowseTheWeb.with(navigator));
        john.wasAbleTo(Open.browserOn(homepage));

        john.whoCan(CallAnApi.at(theRestApiBaseUrl));


        john.attemptsTo(
                Get.resource("/?method=flickr.galleries.getList&api_key=5f0d2221a085fa8b661905738c5b0526&format=json&nojsoncallback=1&auth_token=72157720828805265-b061913ffb29d500&api_sig")
        );

        assertThat(SerenityRest.lastResponse().statusCode()).isEqualTo(200);


    }
    @When("John types his credentials Email and Password")
    public void johnTypesHisCredentialsEmailAndPassword () {
        /*john.attemptsTo(
                Login.words(credential.getEmail(), credential.getPassword())

        );
        readCSVFile.ReadCSVFile();
        System.out.println( "Email: " + credential.getEmail() + " Password: " + credential.getPassword());
    }
    @Then("John gets a wrong message.")
    public void johnGetsAWrongMessage() {
        //actor.should(seeThat(the(homePage.COMPARISON_ELEMENT), containsText("cheese") ));
    }



}
*/